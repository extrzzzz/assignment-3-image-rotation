#include <stdio.h>
#pragma once

enum file_close_status {

    FILE_CLOSE_SUCCESS = 0,
    FILE_CLOSE_ERROR

};
enum file_read_status {

    FILE_OPEN_SUCCESS = 0,
    FILE_OPEN_ERROR,

};

enum file_read_status open_file(char const * const filename, FILE** fp, char const * const open_mode);

enum file_close_status close_file(FILE* fp);

