#include <stdint.h>
#pragma once


struct image {
	uint64_t width, height;
	struct pixel *data;
};
struct pixel { uint8_t b, g, r; };

struct image* create_img(uint64_t width, uint64_t height);

void free_img(struct image *img);
