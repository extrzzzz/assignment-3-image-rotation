#pragma once

enum read_status {
	READ_OK,
    READ_BYTES_ERROR,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE

};
enum write_status {
	WRITE_SUCCESS = 0,
    WRITE_HEADER_ERROR

};

