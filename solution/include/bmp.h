#include <file.h>
#include <image.h>
#include <status.h>
#pragma once
#pragma pack(push, 1)

#define BF_TYPE 0x4d42
#define BI_SIZE 40
#define BIT_COUNT 24

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp(FILE* const in, struct image** const img);

enum write_status to_bmp(FILE* const out, struct image* const img);
