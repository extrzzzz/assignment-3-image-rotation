#include <bmp.h>

static long padding(const uint64_t width)
{
    long pad = 4 - (long)(width * sizeof(struct pixel)) % 4;
    if (pad == 4) {
        pad = 0;
    }
    return pad;
}


enum read_status from_bmp(FILE* const in, struct image** const img) {
    struct bmp_header header;
    if (!fread(&header, sizeof(struct bmp_header), 1, in)){
        return READ_INVALID_HEADER;
    }
    *img = create_img(header.biWidth, header.biHeight);
    long pad = padding(header.biWidth);
    for (size_t i = 0; i < header.biHeight; i++) {
            if(!fread(((*img)->data + i*((*img)->width)), sizeof (struct pixel), (*img)->width, in)){
                free_img(*img);
                return READ_BYTES_ERROR;
            }
            if(fseek(in, pad, SEEK_CUR) != 0){
                free_img(*img);
                return READ_INVALID_SIGNATURE;
            }
    }
    return READ_OK;
}


static struct bmp_header create_bmp_header(uint64_t width, uint64_t height)
{
	struct bmp_header header =
	{
            .bfType = BF_TYPE,
            .bfileSize = width * height * sizeof(struct pixel) + padding(width) * height + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = (width + padding(width)) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

	};
    return header;
}
enum write_status to_bmp(FILE* const out, struct image* const img) {
    struct bmp_header header = create_bmp_header(img->width, img->height);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_HEADER_ERROR;
    }
    long pad = padding(img->width);
    for (uint64_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, 3, img->width, out);
        if(pad != 0){
            fseek(out, pad, SEEK_CUR);
        }
    }

    return WRITE_SUCCESS;
}
