#include <bmp.h>
#include <transform.h>
int main(int argc, char** argv) 
{   
    if (argc != 3){
        printf("Program requires 2 agrguments");
        return -1;
    }
    FILE *in;
    FILE *out;
    open_file(argv[1],&in, "rb");
    open_file(argv[2],&out, "wb");

    struct image *image = NULL;
    
    if(from_bmp(in, &image) != READ_OK){
        printf("Read error.");
        return -1;
    }

    struct image* rotated = rotate(image);
    free_img(image);
    
    if(to_bmp(out, rotated) != WRITE_SUCCESS){
        printf("Write error.");
        free_img(rotated);
        return -1;
    }
    free_img(rotated);
    close_file(in);
    close_file(out);
    return 0;

}
