#include <file.h>

enum file_read_status open_file(char const * const filename, FILE** fp, char const * const open_mode)
{
    *fp = fopen(filename, open_mode);
    if (*fp == NULL)
    {
        return FILE_OPEN_ERROR;
    }
    return FILE_OPEN_SUCCESS;
}

enum file_close_status close_file(FILE* fp)
{
    fclose(fp);
    if (ferror(fp))
    {
        return FILE_CLOSE_ERROR;
    }

    return FILE_CLOSE_SUCCESS;
}

