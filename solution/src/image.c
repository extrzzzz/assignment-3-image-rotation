#include <image.h>
#include <malloc.h>

struct image* create_img(uint64_t width, uint64_t height)
    {
        struct image *img = malloc(sizeof(struct image));
        if (img != NULL){
              img->width = width;
              img->height = height;
              img->data = malloc(width * height * sizeof(struct pixel));
        }
        return img;
    }


void free_img(struct image *img) 
{
    free(img->data);
    free(img);
}
