#include <image.h>

struct image *rotate(struct image const * const image){
    struct image* rotated = create_img(image->height, image->width);
    for (uint64_t i = 0; i < image->width; i++){
        for(uint64_t j = 0; j < image->height; j++){
            rotated->data[i*image->height + j] = image->data[i+image->height*image->width - image->width - j*image->width];
        }
    }
    return rotated;
}

